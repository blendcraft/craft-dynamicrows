# Dynamic Row Settings plugin for Craft CMS 3.x

This plugin will force a 'Row Settings' block into a Dynamic Content Area matrix setup.

## Requirements

This plugin requires Craft CMS 3.0.0-beta.23 or later.

## Installation

To install the plugin, follow these instructions.

1. Open your terminal and go to your Craft project:

        cd /path/to/project

2. Then tell Composer to load the plugin:

        composer require pageworks/dynamic-row-settings

3. In the Control Panel, go to Settings → Plugins and click the “Install” button for Dynamic Row Settings.

## Dynamic Row Settings Overview

-Insert text here-

## Configuring Dynamic Row Settings

-Insert text here-

## Using Dynamic Row Settings

-Insert text here-