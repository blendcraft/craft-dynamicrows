<?php
/**
 * Dynamic Row Settings plugin for Craft CMS 3.x
 *
 * This plugin will force a 'Row Settings' block into a Dynamic Content Area matrix setup.
 *
 * @link      http://www.gamesbykyle.com
 * @copyright Copyright (c) 2018 Kyle Andrews
 */

/**
 * Dynamic Row Settings config.php
 *
 * This file exists only as a template for the Dynamic Row Settings settings.
 * It does nothing on its own.
 *
 * Don't edit this file, instead copy it to 'craft/config' as 'dynamic-row-settings.php'
 * and make your changes there to override default settings.
 *
 * Once copied to 'craft/config', this file will be multi-environment aware as
 * well, so you can have different settings groups for each environment, just as
 * you do for 'general.php'
 */

return [

    // This controls blah blah blah
    "dcaFieldHandles" => true,
    "dcaBlockHandles" => true,

];
