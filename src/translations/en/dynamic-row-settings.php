<?php
/**
 * Dynamic Row Settings plugin for Craft CMS 3.x
 *
 * This plugin will force a 'Row Settings' block into a Dynamic Content Area matrix setup.
 *
 * @link      http://www.gamesbykyle.com
 * @copyright Copyright (c) 2018 Kyle Andrews
 */

/**
 * Dynamic Row Settings en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('dynamic-row-settings', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Kyle Andrews
 * @package   DynamicRowSettings
 * @since     1.0.0
 */
return [
    'Dynamic Row Settings plugin loaded' => 'Dynamic Row Settings plugin loaded',
];
